<?php

require __DIR__ . '/theme-init.php';

// Site navigation support
add_theme_support('menus');
register_nav_menu('sitenav', 'Site navigation');
